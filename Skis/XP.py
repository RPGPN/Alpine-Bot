# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import discord
from discord.ext import commands
import asyncio
from Skis import Tools

def setup(bot):
    bot.add_cog(XP(bot))

class XP:
    def __init__(self, bot):
        self.bot = bot
        self.settings = self.bot.get_cog("Settings")
        self.visors = self.bot.get_cog("Visors")

    def find_xp_member(self, user, server):
        server_members = self.settings.get_server_stat("members", server)

        user_spl = str(user).split("#")
        user_name = user_spl[0]
        user_disc = user_spl[1]

        for key, member in server_members.items():
            mem_spl = member["name"].split("#")
            mem_name = mem_spl[0]
            mem_disc = mem_spl[1]

            if mem_disc == user_disc:
                if mem_name == user_name:
                    return key



    @commands.group(pass_context=True)
    async def xp(self, ctx):
        """General Xp Commands"""
        if ctx.invoked_subcommand is None:
            self.bot.say(
                "Usage: " + self.settings.get_stat("prefix") + "xp give [User] [XP Amount]")

    @xp.command(pass_context=True)
    async def addrank(self, ctx, xp:int=None, role:str=None):
        if not self.visors.is_server_admin(ctx.message.author, ctx.message.server):
            await self.bot.say("Sorry. You're Not An Admin")
        else:
            if xp is None:
                await self.bot.say("Specify an amount of XP")
            if role is None:
                await self.bot.say("Specify a role to assign")
            else:
                ranks = self.settings.get_server_stat("roles", ctx.message.server)
                ranks.append({"xp": xp, "rank": role})
                self.settings.set_server_stat("roles", ctx.message.server, ranks)
                print(self.settings.get_server_stat("roles", ctx.message.server))


    @xp.command(pass_context=True)
    async def give(self, ctx, user = None, amount: int = None):
        """Give Someone XP"""
        if amount < 0 :
            await self.bot.say("You can't give negative xp!")
        elif amount == 0:
            await self.bot.say("Generous of you!")
            return

        name = Tools.find_member(user, ctx.message.server)
        xp_member = self.find_xp_member(name, ctx.message.server)

        if xp_member == ctx.message.author.id:
            await self.bot.say("You can't give yourself xp!")
            return

        give_reserve = self.settings.get_member_stat("reserve", ctx.message.server, ctx.message.author.id)

        if amount > give_reserve:
            await self.bot.say("You can't give xp that you dont have!")
            return

        user_current = self.settings.get_member_stat("exp", ctx.message.server, xp_member)
        user_new_xp = user_current + amount

        self.settings.set_member_stat("exp", ctx.message.server, xp_member, user_new_xp)

        reserve_current = self.settings.get_member_stat("reserve", ctx.message.server, xp_member)
        reserve_new = reserve_current - amount
        self.settings.set_member_stat("reserve", ctx.message.server, ctx.message.author.id, reserve_new)

        await self.bot.say(self.settings.get_member_stat("exp", ctx.message.server, xp_member))

    async def give_reserve(self, user=None, amount:int=None, server=None):
        if amount < 0 :
            return "You can't give negative xp!"
        elif amount == 0:
            return "You can't give 0 xp!"
        if server is None:
            return "Server is Null"
        name = Tools.find_member(user, server)
        xp_member = self.find_xp_member(name, server)

        reserve_current = self.settings.get_member_stat("reserve", server, xp_member)
        reserve_new = reserve_current + amount
        self.settings.set_member_stat("reserve", server, user, reserve_new)