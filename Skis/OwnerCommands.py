# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import discord
from discord.ext import commands
import os
import asyncio


class Owner:
    def __init__(self, bot):
        self.bot = bot
        self.settings = self.bot.get_cog("Settings")
        self.visors = self.bot.get_cog("Visors")

    @commands.command(pass_context=True)
    async def reboot(self, ctx):
        if self.visors.is_owner(ctx.message.author):
            self.settings.flush_to_disk()
            await self.bot.say("Flushed settings to disk.\nRebooting...")
            for task in asyncio.Task.all_tasks():
                try:
                    task.cancel()
                except:
                    continue
            try:
                await self.bot.logout()
                self.bot.loop.stop()
                self.bot.loop.close()
            except:
                pass
            # Kill this process
            os._exit(2)
        else:
            await self.bot.say("Oops! You're Not My **Real** Owner")

    @commands.command(pass_context=True)
    async def setprefix(self, ctx, prfx: str = None):
        """Change the prefix (Owner-Only)
        Requires restart"""
        if self.visors.is_owner(ctx.message.author):
            if prfx is None:
                await self.bot.say("Usage: " + self.settings.get_stat("prefix") + "setprefix [prefix]")
            else:
                self.settings.set_stat('prefix', prfx)
                await self.bot.say("Prefix is " + prfx)
                print(self.settings.getStat('prefix'))
        else:
            await self.bot.say("Oops! You're Not My **Real** Owner")

    @commands.command(pass_context=True)
    async def flush(self, ctx):
        if self.visors.is_owner(ctx.message.author):
            self.settings.flush_to_disk()
        else:
            await self.bot.say("Oops! You're Not My **Real** Owner")

    @commands.command(pass_context=True)
    async def shutdown(self, ctx):
        if self.visors.is_owner(ctx.message.author):
            self.settings.flush_to_disk()
            await self.bot.say("Flushed settings to disk.\nShutting Down...")
            for task in asyncio.Task.all_tasks():
                try:
                    task.cancel()
                except:
                    continue
            try:
                await self.bot.logout()
                self.bot.loop.stop()
                self.bot.loop.close()
            except:
                pass
            # Kill this process
            os._exit(3)
        else:
            await self.bot.say("Oops! You're Not My **Real** Owner")

def setup(bot):
    bot.add_cog(Owner(bot))