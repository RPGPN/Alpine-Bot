# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# Just going on the record : This is admin / owner checks!
import discord
from discord.ext import commands
import os
import asyncio


class Visors:
    def __init__(self, bot):
        self.bot = bot
        self.settings = self.bot.get_cog("Settings")

    def is_owner(self, member):
        if member.id in self.settings.get_stat('owners'):
            return True
        else:
            return False

    def is_server_admin(self, member, server):
        if member.id in self.settings.get_server_stat('admins', server):
            return True
        else:
            return False

def setup(bot):
    bot.add_cog(Visors(bot))