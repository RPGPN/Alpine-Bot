# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import math
import base64
import datetime

def getReadableTimeBetween(first, last):
    # A helper function to make a readable string between two times
    timeBetween = int(last - first)
    weeks = int(timeBetween / 604800)
    days = int((timeBetween - (weeks * 604800)) / 86400)
    hours = int((timeBetween - (days * 86400 + weeks * 604800)) / 3600)
    minutes = int((timeBetween - (hours * 3600 + days * 86400 + weeks * 604800)) / 60)
    seconds = int(timeBetween - (minutes * 60 + hours * 3600 + days * 86400 + weeks * 604800))
    msg = ""

    if weeks > 0:
        if weeks == 1:
            msg = '{}{} week, '.format(msg, str(weeks))
        else:
            msg = '{}{} weeks, '.format(msg, str(weeks))
    if days > 0:
        if days == 1:
            msg = '{}{} day, '.format(msg, str(days))
        else:
            msg = '{}{} days, '.format(msg, str(days))
    if hours > 0:
        if hours == 1:
            msg = '{}{} hour, '.format(msg, str(hours))
        else:
            msg = '{}{} hours, '.format(msg, str(hours))
    if minutes > 0:
        if minutes == 1:
            msg = '{}{} minute, '.format(msg, str(minutes))
        else:
            msg = '{}{} minutes, '.format(msg, str(minutes))
    if seconds > 0:
        if seconds == 1:
            msg = '{}{} second, '.format(msg, str(seconds))
        else:
            msg = '{}{} seconds, '.format(msg, str(seconds))

    if not msg:
        return "0 seconds"
    else:
        return msg[:-2]


def makeBar(progress):
    # Get the progress in half for a shorter progress bar
    shortProgress = progress / 2
    # Convert progress to a string while we are at it
    progressString = str(progress)

    # Get the amount of "done" progress, or the % of 100%
    doneProgress = int(shortProgress)
    # Get the reverse of above, for the % of 100% not done
    undoneProgress = 50-int(shortProgress)

    # We fill the percentage done with # characters
    doneString = '#'*doneProgress
    # The rest with whitespaces
    undoneString = ' '*undoneProgress

    # Build our progress bar and return it
    return '[{}{}] {}%'.format(doneString, undoneString, progressString.rjust(5))

def convert_size(size_bytes):
    if size_bytes == 0:
        return '0B'
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes/p, 2)
    return '%s %s' % (s, size_name[i])


def find_member(name, server):
    name_parts = name.split("#")

    # print(name_parts, " - ", name)
    for member in server.members:
        if member.nick:
            if member.nick.lower() == name.lower():
                return member

    for member in server.members:
        if member.name.lower() == name.lower():
            return member

    if len(name_parts) == 2:
        # print("Name: ",name_parts)
        # Name + Discrim
        try:
            mem_name = name_parts[0]
            mem_disc = int(name_parts[1])
        except:
            mem_name = mem_disc = None
        if mem_name:
            for member in server.members:
                if member.name.lower() == mem_name.lower() and int(member.discriminator) == mem_disc:
                    return member

    print(name,server)