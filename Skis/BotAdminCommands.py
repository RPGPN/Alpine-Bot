# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import discord
from discord.ext import commands
import os
import asyncio

def setup(bot):
    bot.add_cog(BotAdmin(bot))

class BotAdmin:
    def __init__(self, bot):
        self.bot = bot
        self.settings = self.bot.get_cog("Settings")
        self.visors = self.bot.get_cog("Visors")