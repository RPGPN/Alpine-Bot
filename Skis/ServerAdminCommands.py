# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import discord
from discord.ext import commands
import os
import asyncio


def setup(bot):
    bot.add_cog(ServerAdmin(bot))


class ServerAdmin:
    def __init__(self, bot):
        self.bot = bot
        self.settings = self.bot.get_cog("Settings")
        self.visors = self.bot.get_cog("Visors")

    @commands.command(pass_context=True)
    async def setdefaultchannel(self, ctx, deschan = None):
        '''Set the server's default channel'''
        if ctx.message.server is None:
            await self.bot.say("Sorry. This command must be run in a server")
            return
        if self.visors.is_server_admin(ctx.message.author, ctx.message.server):
            tchan = discord.utils.get(ctx.message.server.channels, name=deschan)
            self.settings.set_server_stat("defaultchannel", ctx.message.server, tchan.id)
            await self.bot.say("Sure! Default channel is now {}({})".format(tchan, tchan.id))
        else:
            await self.bot.say("Sorry. You're Not An Admin")

    @commands.command(pass_context=True)
    async def setwelcomechannel(self, ctx, deschan=None):
        '''Set the server's welcome channel'''
        if ctx.message.server is None:
            await self.bot.say("Sorry. This command must be run in a server")
            return
        if self.visors.is_server_admin(ctx.message.author, ctx.message.server):
            tchan = discord.utils.get(ctx.message.server.channels, name=deschan)
            self.settings.set_server_stat("welcomechannel", ctx.message.server, tchan.id)
            await self.bot.say("Sure! Welcome channel is now {}({})".format(tchan, tchan.id))
        else:
            await self.bot.say("Sorry. You're Not An Admin")

    @commands.command(pass_context=True)
    async def addserveradmin(self, ctx, user):
        '''Add an server admin to the list'''
        if ctx.message.server is None:
            await self.bot.say("Sorry. You Need To Be In A Server")
        if self.visors.is_server_admin(ctx.message.author, ctx.message.server):
            u = discord.utils.get(ctx.message.server.members, name=user)
            self.settings.append_server_stat("servers", ctx.message.server, u.id)
            return
        else:
            await self.bot.say("Sorry. You're Not An Admin")

