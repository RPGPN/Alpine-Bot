# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import discord
from discord.ext import commands
import os
import asyncio

import psutil
import sys
import os
import platform
import time

from Skis import Tools


def setup(bot):
    bot.add_cog(Info(bot))


class Info:
    def __init__(self, bot):
        self.bot = bot
        self.settings = self.bot.get_cog("Settings")
        # self.visors = self.bot.get_cog("Visors")

    # @commands.command()
    # async def info(self):
    #     msg = ''

    @commands.command()
    async def hostinfo(self):
        """Get Bot Hosting Stats"""
        # CPU stats.
        cpu_threads = os.cpu_count()
        cpu_usage = psutil.cpu_percent(interval=1)

        # Memory stats.
        mem_stats = psutil.virtual_memory()
        mem_percent = mem_stats.percent
        mem_used = Tools.convert_size(mem_stats.used)
        mem_total = Tools.convert_size(mem_stats.total)

        # Platform info.
        platform_current = platform.platform()

        # Python version info.
        pyver_major = sys.version_info.major
        pyver_minor = sys.version_info.minor
        pyver_micro = sys.version_info.micro
        pyver_release = sys.version_info.releaselevel

        stor = psutil.disk_usage('/')
        stor_used = Tools.convert_size(stor.used)
        stor_total = Tools.convert_size(stor.total)
        stor_free = Tools.convert_size(stor.total - stor.used)

        msg = '***{}\'s*** **Home:**\n'.format(self.bot.user.name)
        msg += '```Host OS       : {}\n'.format(platform_current)
        msg += 'Host Python   : {}.{}.{} {}\n'.format(pyver_major, pyver_minor, pyver_micro, pyver_release)
        if not isinstance(cpu_threads, int):
            msg += 'Host CPU usage: {}% of {}\n'.format(cpu_usage, platform.machine())
        elif cpu_threads > 1:
            msg += 'Host CPU usage: {}% of {} ({} threads)\n'.format(cpu_usage, platform.machine(), cpu_threads)
        else:
            msg += 'Host CPU usage: {}% of {} ({} thread)\n'.format(cpu_usage, platform.machine(), cpu_threads)
        msg += 'Host RAM      : {} ({}%) of {}\n'.format(mem_used, mem_percent, mem_total)
        msg += 'Host storage  : {} ({}%) of {} - {} free\n'.format(stor_used, stor.percent, stor_total, stor_free)
        msg += 'Hostname      : {}\n'.format(platform.node())
        msg += 'Host uptime   : {}```'.format(Tools.getReadableTimeBetween(psutil.boot_time(), time.time()))
        await self.bot.say(msg)

    @commands.command()
    async def cpuinfo(self):
        """Get Bot Host CPU Stats"""
        cpu_pcts = psutil.cpu_percent(interval=0.1, percpu=True)
        cpu_pct_str = '{}\n'.format(platform.processor())
        cpu_threads = psutil.cpu_count()
        cpu_cores = psutil.cpu_count(logical=False)
        cpu_arch = platform.machine()
        # First, check to see if we can accurately determine the number of physical cores. If not, omit the core count.
        if not cpu_cores:
            if cpu_threads > 1:
                cpu_pct_str += '{} threads of {}'.format(cpu_threads, cpu_arch)
            else:
                cpu_pct_str += '{} thread of {}'.format(cpu_threads, cpu_arch)
        elif cpu_cores > 1:  # Multiple cores.
            cpu_pct_str += '{} threads - {} cores of {}'.format(cpu_threads, cpu_cores, cpu_arch)
        else:
            if psutil.cpu_count() > 1:  # Multiple threads, single core.
                cpu_pct_str += '{} threads - {} core of {}'.format(cpu_threads, cpu_cores, cpu_arch)
            else:  # Single thread, single core.
                cpu_pct_str += '{} thread - {} core of {}'.format(cpu_threads, cpu_cores, cpu_arch)

        # Build CPU usage graph.
        cpu_pct_str += '\n\n'
        for index, value in enumerate(cpu_pcts):
            cpu_pct_str += 'CPU {}: {}\n'.format(str(index), Tools.makeBar(cpu_pcts[index]))

        await self.bot.say('```{}```'.format(cpu_pct_str))

    @commands.command()
    async def invite(self):
        """Get A Bot Invite Link"""
        await self.bot.say("Invite Link: https://discordapp.com/oauth2/authorize?client_id={}&scope=bot&permissions=8"
                           .format(self.bot.user.id))

