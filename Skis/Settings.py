# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import json
import discord
from discord.ext import commands
import os
import asyncio
import time


class Settings:
    def __init__(self, bot):
        self.dir_path = os.path.dirname(os.path.realpath(__file__))[:-4]
        self.bot = bot
        self.file = self.dir_path + 'settings.json'
        self.settings_dict = {}
        self.dumptime = 300
        self.default_member = {
            "name": "",
            "exp": 0,
            "reserve": 0,
            "banned": False,
            "xproles": []
        }
        self.default_server = {
            "rules": "",
            "admins": [],
            "members": {},
            "donation_link": "*This Server Does Not Accept Donations*",
            "welcome": "Hello {0.name} !",
            "goodbye": "Hope To See {0.name} Again Soon!",
            "name": "",
            "welcomechannel": "",
            "defaultchannel": "",
            "alps": 0,
            "maxxpreserve": 100,
            "roles": []
        }
        self.default_role = {
            "amount": 0,
            "rank": "",
            "roleid": ""
        }
        self.prefix = '$'
        try:
            self.settings_dict = json.load(open(self.file))
        except FileNotFoundError:
            self.firstTime()

    def firstTime(self):
        temp_s_dict = {
            "presence": "RPGPN",
            "prestype": 3,
            "presurl": "",
            "token": "",
            "prefix": "~",
            "owners": [],
            "admins": [],
            "servers": {},
            "plugins": []
        }
        print("\nFirst Time Setup - Alpine")
        temp_s_dict["token"] = input("Enter Token : ")
        owner = input("Enter Owner ID : ")
        temp_s_dict["owners"].append(owner)
        temp_s_dict["admins"].append(owner)
        json.dump(temp_s_dict, open(self.file, "w"), indent=2)
        asyncio.sleep(2)
        exit(1)

    def flush_to_disk(self):
        json.dump(self.settings_dict, open(self.file, "w"), indent=2)

    def get_stat(self, stat):
        return self.settings_dict[stat]

    def set_stat(self, stat, value):
        self.settings_dict[stat] = value

    def get_server_stat(self, stat, server):
        return self.settings_dict["servers"][str(server.id)][stat]

    def set_server_stat(self, stat, server, value):
        self.settings_dict["servers"][str(server.id)][stat] = value

    def get_member_stat(self, stat, server, memberid):
        return self.settings_dict["servers"][str(server.id)]["members"][str(memberid)][stat]

    def set_member_stat(self, stat, server, memberid, value):
        self.settings_dict["servers"][str(server.id)]["members"][str(memberid)][stat] = value

    def append_server_stat(self, stat, server, value):
        self.settings_dict["servers"][str(server.id)][stat].append(value)
        print(self.settings_dict["servers"][str(server.id)][stat])

    def server_setup(self, server):
        self.settings_dict['servers'][str(server.id)] = {}
        for key in self.default_server:
            if type(self.default_server[key]) == dict:
                self.settings_dict["servers"][str(server.id)][key] = {}
            else:
                self.settings_dict["servers"][str(server.id)][key] = self.default_server[key]
        self.settings_dict['servers'][str(server.id)]['admins'].append(str(server.owner.id))
        self.settings_dict['servers'][str(server.id)]['name'] = str(server)
        if server.default_channel != None:
            self.settings_dict['servers'][str(server.id)]['welcomechannel'] = str(server.default_channel.id)
            self.settings_dict['servers'][str(server.id)]['defaultchannel'] = str(server.default_channel.id)
        self.flush_to_disk()

    def user_setup(self, user, server):
        self.settings_dict['servers'][str(server.id)]["members"][str(user.id)] = {}
        for key in self.default_member:
            if type(self.default_member[key]) == dict:
                self.settings_dict["servers"][str(server.id)]["members"][str(user.id)][key] = {}
            else:
                self.settings_dict["servers"][str(server.id)]["members"][str(user.id)][key] = self.default_member[key]
                self.settings_dict["servers"][str(server.id)]["members"][str(user.id)]['name'] = str(user)


def setup(bot):
    bot.add_cog(Settings(bot))