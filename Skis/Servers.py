# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import discord
from discord.ext import commands
import random
import asyncio


def setup(bot):
    bot.add_cog(Servers(bot))


class Servers:
    def __init__(self, bot):
        self.bot = bot
        self.settings = self.bot.get_cog("Settings")
        self.visors = self.bot.get_cog("Visors")

        self.firstmessage = ["Alright Cupcake", "Hi", "Hello", "Bello"]

    async def init_server(self, server):
        print('Adding Server ' + server.id)
        self.settings.server_setup(server)
        fm = random.choice(self.firstmessage)
        # print(fm)
        msg = "**{}**! I'm Alpine!\nI'm in your server ***{}*** now!\nSo I can finalise some info I would " \
              "appreciate you running `{}alps` in the server"
        await self.bot.send_message(server.owner, msg.format(fm, server.name, self.settings.get_stat("prefix")))

    async def init_server_check(self, server):
        if str(server.id) in self.settings.get_stat("servers"):
            return
        else:
            await self.init_server(server)

    @commands.command(hidden=True, pass_context=True)
    async def alps(self, ctx):
        if ctx.message.server is None:
            await self.bot.say("Sorry. This command must be run in a server")
            return
        if ctx.message.author.id == ctx.message.server.owner.id:
            if self.settings.get_server_stat("alps", ctx.message.server) == 0:
                sid = ctx.message.server.id
                so = ctx.message.server.owner
                print("Started Setup For {} By {}".format(sid, ctx.message.author))
                await self.bot.send_message(so, "Sure! Let's Go!")

                await self.bot.send_message(so, "I am currently unsure on the default channel in your server\nTo fix "
                                                "this, please run `{}setdefaultchannel`"
                                            .format(self.settings.get_stat("prefix")))

                await self.bot.send_message(so, "I am also unsure on the channel in your server to use for my welcomes"
                                                " to new users\nTo fix this, please run `{}setwelcomechannel`"
                                            .format(self.settings.get_stat("prefix")))

                await self.bot.send_message(so, "At any point you can run `{}alpsconfig` to get your server's current "
                                                "configuration".format(self.settings.get_stat("prefix")))

                self.settings.set_server_stat("alps", ctx.message.server, 1)
            else:
                await self.bot.say("Oops! Server already set up! Run `{}alpsconfig` to get your server's current "
                                   "configuration".format(self.settings.get_stat("prefix")))
        else:
            await self.bot.say("Oops! You're Not The Server Owner")

    @commands.command(pass_context=True)
    async def alpsconfig(self, ctx):
        """Get Server's Current Config (Server-Admin Only)"""
        if ctx.message.server is None:
            await self.bot.say("Sorry. This command must be run in a server")
            return
        if self.visors.is_server_admin(ctx.message.author, ctx.message.server):
            msg = '```'
            msg += 'Default Channel: {}'.format(self.settings.get_server_stat("defaultchannel", ctx.message.server))
            msg += '\nWelcome Channel: {}'.format(self.settings.get_server_stat("welcomechannel", ctx.message.server))
            msg += '\n----'
            msg += '\nWelcome Message: {}'.format(self.settings.get_server_stat("welcome", ctx.message.server))
            msg += '\nGoodbye Message: {}'.format(self.settings.get_server_stat("goodbye", ctx.message.server))
            msg += '\n----'
            msg += '\nDonation Link: {}'.format(self.settings.get_server_stat("donation_link", ctx.message.server))
            msg += '```'
            await self.bot.send_message(ctx.message.author, msg)
        else:
            await self.bot.say("Sorry. You're Not An Admin")

    def check_user(self, user, server):
        if user.id in self.settings.get_server_stat("members", server):
            return
        else:
            self.settings.user_setup(user, server)
