# RPGPN Alpine
Welcome to Alpine, our new [Discord](https://discordapp.com) bot
[![forthebadge](http://forthebadge.com/images/badges/made-with-python.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/designed-in-etch-a-sketch.svg)](http://forthebadge.com)

## Installation

### General
Please, please, just [invite](https://discordapp.com/oauth2/authorize?client_id=406808100667129857&scope=bot&permissions=8) the bot to your server rather than hosting your own, the hosted version will contain the latest updates.
