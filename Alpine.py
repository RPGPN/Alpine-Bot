# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import discord
from discord.ext import commands
import logging


def logSetup():
    logger = logging.getLogger('discord')
    logger.setLevel(logging.DEBUG)
    handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
    handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
    logger.addHandler(handler)


def loadCogs():
    bot.load_extension("Skis.Settings")  # Me First.
    bot.load_extension("Skis.Visors")  # %Settings

    bot.load_extension("Skis.OwnerCommands")  # Now Me. %Visors, Settings
    bot.load_extension("Skis.ServerAdminCommands")  # Now Me. %Visors, Settings
    bot.load_extension("Skis.BotAdminCommands")  # Now Me. %Visors, Settings

    bot.load_extension("Skis.Servers")  # Now Me. %Settings
    bot.load_extension("Skis.Info")  # Now Me. %Settings
    bot.load_extension("Skis.XP")  # Now Me. %Settings
    # bot.load_extension("Skis.Settings")


logSetup()
bot = commands.Bot(command_prefix=commands.when_mentioned_or('~'), description="Alpine", pm_help=True)
loadCogs()
settings = bot.get_cog("Settings")
serverscog = bot.get_cog("Servers")

@bot.event
async def on_ready():
    print("Logged in as " + str(bot.user.name) + " (" + str(bot.user.id) + ")")
    if settings.get_stat('presurl') is None:
        url = None
        await bot.change_presence(game=discord.Game(name=settings.get_stat('presence'), type=int(settings.get_stat('prestype'))))
    else:
        await bot.change_presence(game=discord.Game(name=settings.get_stat('presence'), type=int(settings.get_stat('prestype')), url=settings.get_stat('presurl')))
    print("Bot Invite Link: " + "https://discordapp.com/oauth2/authorize?client_id=" + bot.user.id + "&scope=bot&permissions=8")

    # Go into a loop to check for new servers while we were away
    for server in bot.servers:
        await serverscog.init_server_check(server)
        for member in server.members:
            serverscog.check_user(member, server)

@bot.event
async def on_server_join(server):
    await serverscog.init_server_check(server)

@bot.event
async def on_member_join(member):
    settings.user_setup(member, member.server)

bot.run(settings.get_stat("token"))